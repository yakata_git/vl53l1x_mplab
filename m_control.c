#include "mcc_generated_files/mcc.h"
#include "vl53l1x.h"
#include "m_control.h"
#include <math.h>


uint16_t data;

uint16_t TARGET_DIST=250;//target distance from wall
uint16_t ERROR_DIST=50; //acceptable diatance from wal to target distance, if error is 50, target +50mm or -50mm is acceptable 

void m_control_init()
{
    carstate=STOP;
    LINE2 = TARGET_DIST+ERROR_DIST;//300
    LINE1 = TARGET_DIST-ERROR_DIST;//200 between 200 to 300 is OK
    
    PWM5_LoadDutyValue(0);
    PWM6_LoadDutyValue(0);
}

void m_control_switch_on()
{
    LED_SetHigh();
    __delay_ms(1000);
    LED_SetLow();
    mainSW = ~mainSW;
}

void m_control_run()
{
    CUR_DIST = get_distance_to_wall();
    m_set_speed();
    
    
    if(CUR_DIST > LINE2) //car position is too far from target
    {
        m_statecheck(FORWARD);//check brake is neccesary or not
        m_go_forward();
    }
    else if((CUR_DIST > LINE1) & (CUR_DIST <= LINE2) )
    {
        m_brake();
    }
    else    //car position is too close to wall
    {
        m_statecheck(BACKWARD);//check brake is neccesary or not
        m_go_backward();
    }

}
void m_set_speed()
{
   
    CUR_DIST_TO_TARGET = get_distnace_to_target();
    if(CUR_DIST_TO_TARGET < 400)
    {
        SPEED =380;//max SPEED is 0xf00 = 3840
    }
    else
    {
        SPEED = 3840;
    }
}
void m_statecheck(CAR_STATE newstate)
{
    if(carstate != newstate) //ex. FORWARD != REVERSE
    {
        m_brake(); // at changing action anyway brake
        if(newstate==FORWARD)
        {
            
        }
        if(newstate==BACKWARD)
        {
            
        }
    } 
    carstate = newstate;
}
uint16_t get_distnace_to_target()
{
    return abs(CUR_DIST -TARGET_DIST);
}

void m_brake()
{
    PWM5_LoadDutyValue(0xf00);
    PWM6_LoadDutyValue(0xf00);
    __delay_ms(500);
}

void m_go_forward()
{
    PWM6_LoadDutyValue(0x000);
    PWM5_LoadDutyValue(SPEED);
}
void m_go_backward()
{
    PWM5_LoadDutyValue(0x000);
    PWM6_LoadDutyValue(SPEED);
}

uint16_t get_distance_to_wall()
{
    uint16_t ave = 0;
    int i;
    for (i = 0; i < 8; i++) {
        ave += VL53L1X_read();
    }
    ave = (ave >> 3);
    if(ave > 3500)
    {
        ave = 3500;
    }
    return (ave);
    
}

int16_t expo(int16_t x)
{
    unsigned char i,j;
    float a=1;

    unsigned long int fact=1;
    float result=1.0;
    for(i=1;i<=10;i++)
    {
        fact=fact*i;
        a = a * x;
        result=result + ((float)a/(float)fact);
    }
    return result;
}

void setVelocity(int16_t val)
{
    if(val > 0)
    {//forward
//        FIN_SetHigh();
//        RIN_SetLow();
        PWM6_LoadDutyValue(0);
        if(abs(val) > 0x3ff)
        {
            PWM5_LoadDutyValue(0x3ff);
            //PWM6_LoadDutyValue(0);
        }
        else
        {
            PWM5_LoadDutyValue(val);
            //PWM6_LoadDutyValue(0);
        }
    }
    else
    {//backward
        //FIN_SetLow();
        //RIN_SetHigh();
        PWM5_LoadDutyValue(0);
        if(abs(val) > 0x3ff)
        {
            PWM6_LoadDutyValue(0x3ff);
        }
        else
        {
            PWM6_LoadDutyValue(abs(val));
        }
    }
}