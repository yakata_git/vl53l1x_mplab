/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.6
        Device            :  PIC16F18326
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.30 and above
        MPLAB 	          :  MPLAB X 5.40	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set RA4 procedures
#define RA4_SetHigh()            do { LATAbits.LATA4 = 1; } while(0)
#define RA4_SetLow()             do { LATAbits.LATA4 = 0; } while(0)
#define RA4_Toggle()             do { LATAbits.LATA4 = ~LATAbits.LATA4; } while(0)
#define RA4_GetValue()              PORTAbits.RA4
#define RA4_SetDigitalInput()    do { TRISAbits.TRISA4 = 1; } while(0)
#define RA4_SetDigitalOutput()   do { TRISAbits.TRISA4 = 0; } while(0)
#define RA4_SetPullup()             do { WPUAbits.WPUA4 = 1; } while(0)
#define RA4_ResetPullup()           do { WPUAbits.WPUA4 = 0; } while(0)
#define RA4_SetAnalogMode()         do { ANSELAbits.ANSA4 = 1; } while(0)
#define RA4_SetDigitalMode()        do { ANSELAbits.ANSA4 = 0; } while(0)

// get/set VREF aliases
#define VREF_TRIS                 TRISAbits.TRISA5
#define VREF_LAT                  LATAbits.LATA5
#define VREF_PORT                 PORTAbits.RA5
#define VREF_WPU                  WPUAbits.WPUA5
#define VREF_OD                   ODCONAbits.ODCA5
#define VREF_ANS                  ANSELAbits.ANSA5
#define VREF_SetHigh()            do { LATAbits.LATA5 = 1; } while(0)
#define VREF_SetLow()             do { LATAbits.LATA5 = 0; } while(0)
#define VREF_Toggle()             do { LATAbits.LATA5 = ~LATAbits.LATA5; } while(0)
#define VREF_GetValue()           PORTAbits.RA5
#define VREF_SetDigitalInput()    do { TRISAbits.TRISA5 = 1; } while(0)
#define VREF_SetDigitalOutput()   do { TRISAbits.TRISA5 = 0; } while(0)
#define VREF_SetPullup()          do { WPUAbits.WPUA5 = 1; } while(0)
#define VREF_ResetPullup()        do { WPUAbits.WPUA5 = 0; } while(0)
#define VREF_SetPushPull()        do { ODCONAbits.ODCA5 = 0; } while(0)
#define VREF_SetOpenDrain()       do { ODCONAbits.ODCA5 = 1; } while(0)
#define VREF_SetAnalogMode()      do { ANSELAbits.ANSA5 = 1; } while(0)
#define VREF_SetDigitalMode()     do { ANSELAbits.ANSA5 = 0; } while(0)

// get/set RC0 procedures
#define RC0_SetHigh()            do { LATCbits.LATC0 = 1; } while(0)
#define RC0_SetLow()             do { LATCbits.LATC0 = 0; } while(0)
#define RC0_Toggle()             do { LATCbits.LATC0 = ~LATCbits.LATC0; } while(0)
#define RC0_GetValue()              PORTCbits.RC0
#define RC0_SetDigitalInput()    do { TRISCbits.TRISC0 = 1; } while(0)
#define RC0_SetDigitalOutput()   do { TRISCbits.TRISC0 = 0; } while(0)
#define RC0_SetPullup()             do { WPUCbits.WPUC0 = 1; } while(0)
#define RC0_ResetPullup()           do { WPUCbits.WPUC0 = 0; } while(0)
#define RC0_SetAnalogMode()         do { ANSELCbits.ANSC0 = 1; } while(0)
#define RC0_SetDigitalMode()        do { ANSELCbits.ANSC0 = 0; } while(0)

// get/set RC1 procedures
#define RC1_SetHigh()            do { LATCbits.LATC1 = 1; } while(0)
#define RC1_SetLow()             do { LATCbits.LATC1 = 0; } while(0)
#define RC1_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define RC1_GetValue()              PORTCbits.RC1
#define RC1_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define RC1_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)
#define RC1_SetPullup()             do { WPUCbits.WPUC1 = 1; } while(0)
#define RC1_ResetPullup()           do { WPUCbits.WPUC1 = 0; } while(0)
#define RC1_SetAnalogMode()         do { ANSELCbits.ANSC1 = 1; } while(0)
#define RC1_SetDigitalMode()        do { ANSELCbits.ANSC1 = 0; } while(0)

// get/set LED aliases
#define LED_TRIS                 TRISCbits.TRISC3
#define LED_LAT                  LATCbits.LATC3
#define LED_PORT                 PORTCbits.RC3
#define LED_WPU                  WPUCbits.WPUC3
#define LED_OD                   ODCONCbits.ODCC3
#define LED_ANS                  ANSELCbits.ANSC3
#define LED_SetHigh()            do { LATCbits.LATC3 = 1; } while(0)
#define LED_SetLow()             do { LATCbits.LATC3 = 0; } while(0)
#define LED_Toggle()             do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define LED_GetValue()           PORTCbits.RC3
#define LED_SetDigitalInput()    do { TRISCbits.TRISC3 = 1; } while(0)
#define LED_SetDigitalOutput()   do { TRISCbits.TRISC3 = 0; } while(0)
#define LED_SetPullup()          do { WPUCbits.WPUC3 = 1; } while(0)
#define LED_ResetPullup()        do { WPUCbits.WPUC3 = 0; } while(0)
#define LED_SetPushPull()        do { ODCONCbits.ODCC3 = 0; } while(0)
#define LED_SetOpenDrain()       do { ODCONCbits.ODCC3 = 1; } while(0)
#define LED_SetAnalogMode()      do { ANSELCbits.ANSC3 = 1; } while(0)
#define LED_SetDigitalMode()     do { ANSELCbits.ANSC3 = 0; } while(0)

// get/set SW aliases
#define SW_TRIS                 TRISCbits.TRISC4
#define SW_LAT                  LATCbits.LATC4
#define SW_PORT                 PORTCbits.RC4
#define SW_WPU                  WPUCbits.WPUC4
#define SW_OD                   ODCONCbits.ODCC4
#define SW_ANS                  ANSELCbits.ANSC4
#define SW_SetHigh()            do { LATCbits.LATC4 = 1; } while(0)
#define SW_SetLow()             do { LATCbits.LATC4 = 0; } while(0)
#define SW_Toggle()             do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define SW_GetValue()           PORTCbits.RC4
#define SW_SetDigitalInput()    do { TRISCbits.TRISC4 = 1; } while(0)
#define SW_SetDigitalOutput()   do { TRISCbits.TRISC4 = 0; } while(0)
#define SW_SetPullup()          do { WPUCbits.WPUC4 = 1; } while(0)
#define SW_ResetPullup()        do { WPUCbits.WPUC4 = 0; } while(0)
#define SW_SetPushPull()        do { ODCONCbits.ODCC4 = 0; } while(0)
#define SW_SetOpenDrain()       do { ODCONCbits.ODCC4 = 1; } while(0)
#define SW_SetAnalogMode()      do { ANSELCbits.ANSC4 = 1; } while(0)
#define SW_SetDigitalMode()     do { ANSELCbits.ANSC4 = 0; } while(0)

// get/set RC5 procedures
#define RC5_SetHigh()            do { LATCbits.LATC5 = 1; } while(0)
#define RC5_SetLow()             do { LATCbits.LATC5 = 0; } while(0)
#define RC5_Toggle()             do { LATCbits.LATC5 = ~LATCbits.LATC5; } while(0)
#define RC5_GetValue()              PORTCbits.RC5
#define RC5_SetDigitalInput()    do { TRISCbits.TRISC5 = 1; } while(0)
#define RC5_SetDigitalOutput()   do { TRISCbits.TRISC5 = 0; } while(0)
#define RC5_SetPullup()             do { WPUCbits.WPUC5 = 1; } while(0)
#define RC5_ResetPullup()           do { WPUCbits.WPUC5 = 0; } while(0)
#define RC5_SetAnalogMode()         do { ANSELCbits.ANSC5 = 1; } while(0)
#define RC5_SetDigitalMode()        do { ANSELCbits.ANSC5 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);


/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handler for the IOCCF4 pin functionality
 * @Example
    IOCCF4_ISR();
 */
void IOCCF4_ISR(void);

/**
  @Summary
    Interrupt Handler Setter for IOCCF4 pin interrupt-on-change functionality

  @Description
    Allows selecting an interrupt handler for IOCCF4 at application runtime
    
  @Preconditions
    Pin Manager intializer called

  @Returns
    None.

  @Param
    InterruptHandler function pointer.

  @Example
    PIN_MANAGER_Initialize();
    IOCCF4_SetInterruptHandler(MyInterruptHandler);

*/
void IOCCF4_SetInterruptHandler(void (* InterruptHandler)(void));

/**
  @Summary
    Dynamic Interrupt Handler for IOCCF4 pin

  @Description
    This is a dynamic interrupt handler to be used together with the IOCCF4_SetInterruptHandler() method.
    This handler is called every time the IOCCF4 ISR is executed and allows any function to be registered at runtime.
    
  @Preconditions
    Pin Manager intializer called

  @Returns
    None.

  @Param
    None.

  @Example
    PIN_MANAGER_Initialize();
    IOCCF4_SetInterruptHandler(IOCCF4_InterruptHandler);

*/
extern void (*IOCCF4_InterruptHandler)(void);

/**
  @Summary
    Default Interrupt Handler for IOCCF4 pin

  @Description
    This is a predefined interrupt handler to be used together with the IOCCF4_SetInterruptHandler() method.
    This handler is called every time the IOCCF4 ISR is executed. 
    
  @Preconditions
    Pin Manager intializer called

  @Returns
    None.

  @Param
    None.

  @Example
    PIN_MANAGER_Initialize();
    IOCCF4_SetInterruptHandler(IOCCF4_DefaultInterruptHandler);

*/
void IOCCF4_DefaultInterruptHandler(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/